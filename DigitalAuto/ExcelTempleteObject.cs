﻿namespace DigitalAuto.Model.Templete {

    public struct ExcelDefaultObj {
        public string date { get; set; }
        public string dateForm { get; set; }
        public string dateSheetNameForm {  get; set; }
        public CornerAddress corner { get; set; }
        public AgeGenderAddress age_gender { get; set; }
    }

    public struct ExcelTotalObj {
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string fromDateForm { get; set; }
        public string toDateForm { get; set; }
        public string sheetName { get; set;}
        public CornerAddress corner { get; set; }
        public AgeGenderAddress age_gender { get; set; }
    }

    public class ExcelTempleteObject {
        public ExcelPropertiesObj properties { get; set; }

        public class ExcelPropertiesObj {
            public ExcelStyleObj style { get; set; }
            public ExcelFormatObj format { get; set; }

            public class ExcelStyleObj {
                public string file { get; set; }
                public uint defaultSheetNo { get; set; }
                public uint totalSheetNo { get; set; }
            }

            public class ExcelFormatObj {
                public ExcelDefaultObj default_form { get; set; }
                public ExcelTotalObj total_form { get; set; }
            }
        }
    }
	
	
}
