﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace DigitalAuto.Model.DAO {
    public class OracleDBManager {

        public class AlreadyConnectedException : Exception { public AlreadyConnectedException(string message) : base(message) { } };
        public class NotConnectedException : Exception { public NotConnectedException(string message) : base(message) { } };

        readonly string ip;
        readonly uint port;
        private OracleConnection oracleConnection;
        private OracleCommand oracleCommand;
        private Boolean isConnected;

        public OracleDBManager(string ip, uint port) {
            this.ip = ip;
            this.port = port;
            this.isConnected = false;
        }

        public void ConnectionDB(string dbUserId, string dbSID, string dbPassword) {
            if (!isConnected) { 
                oracleConnection = new OracleConnection(
                    String.Format(@"Data Source = (DESCRIPTION =
                        (ADDRESS_LIST =
                          (ADDRESS = (PROTOCOL = TCP)(HOST = {0})(PORT = {1}))
                        )
                        (CONNECT_DATA =
                          (SERVER = DEDICATED)
                          (SERVICE_NAME = {2})
                        )
                      );User ID={3};Password={4};",this.ip,this.port,dbSID,dbUserId,dbPassword));
                    oracleConnection.Open();
                    oracleCommand = oracleConnection.CreateCommand();
                    isConnected = true;
            }else{
                throw new AlreadyConnectedException("DB 접속전, 이미 연결된 세션을 종료해야 합니다.");
            }
        }

        public List<String[]> Query(string query) {
            if (this.isConnected) {
                oracleCommand.CommandText = query;
                List<String[]> lists = new List<String[]>();
                using (OracleDataReader reader = oracleCommand.ExecuteReader()) {
                    while (reader.Read()) {
                        string[] row = new string[reader.FieldCount];
                        for (int i = 0; i < row.Length; i++) {
                            row[i] = reader.GetValue(i).ToString();
                        }
                        lists.Add(row);
                    }
                }
                return lists;
            } else {
                throw new NotConnectedException("쿼리를 요청하기 전, 아직 데이터베이스에 연결되지 않았습니다.");
            }

        }

        public void Close() {
            oracleCommand.Cancel();
            oracleConnection.Close();
            isConnected = false;
        }


    }
}
