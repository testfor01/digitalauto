﻿namespace DigitalAuto {
    partial class Digital {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent() {
        	this.label1 = new System.Windows.Forms.Label();
        	this.dateTime_fromDate = new System.Windows.Forms.DateTimePicker();
        	this.dateTime_toDate = new System.Windows.Forms.DateTimePicker();
        	this.label2 = new System.Windows.Forms.Label();
        	this.label3 = new System.Windows.Forms.Label();
        	this.button_createDefault = new System.Windows.Forms.Button();
        	this.button_createTotal = new System.Windows.Forms.Button();
        	this.label_filepath = new System.Windows.Forms.Label();
        	this.menuStrip1 = new System.Windows.Forms.MenuStrip();
        	this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.openExcelToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        	this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.settingsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        	this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
        	this.textBox_stateBox = new System.Windows.Forms.TextBox();
        	this.groupBox1 = new System.Windows.Forms.GroupBox();
        	this.button_DB = new System.Windows.Forms.Button();
        	this.menuStrip1.SuspendLayout();
        	this.groupBox1.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// label1
        	// 
        	this.label1.AutoSize = true;
        	this.label1.Location = new System.Drawing.Point(23, 31);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(112, 12);
        	this.label1.TabIndex = 1;
        	this.label1.Text = "Target_excel_file : ";
        	// 
        	// dateTime_fromDate
        	// 
        	this.dateTime_fromDate.Location = new System.Drawing.Point(80, 309);
        	this.dateTime_fromDate.Name = "dateTime_fromDate";
        	this.dateTime_fromDate.Size = new System.Drawing.Size(200, 21);
        	this.dateTime_fromDate.TabIndex = 2;
        	this.dateTime_fromDate.ValueChanged += new System.EventHandler(this.dateTime_fromDate_ValueChanged);
        	// 
        	// dateTime_toDate
        	// 
        	this.dateTime_toDate.Location = new System.Drawing.Point(80, 345);
        	this.dateTime_toDate.Name = "dateTime_toDate";
        	this.dateTime_toDate.Size = new System.Drawing.Size(200, 21);
        	this.dateTime_toDate.TabIndex = 3;
        	this.dateTime_toDate.ValueChanged += new System.EventHandler(this.dateTime_toDate_ValueChanged);
        	// 
        	// label2
        	// 
        	this.label2.AutoSize = true;
        	this.label2.Location = new System.Drawing.Point(9, 315);
        	this.label2.Name = "label2";
        	this.label2.Size = new System.Drawing.Size(53, 12);
        	this.label2.TabIndex = 4;
        	this.label2.Text = "시작일 : ";
        	// 
        	// label3
        	// 
        	this.label3.AutoSize = true;
        	this.label3.Location = new System.Drawing.Point(9, 351);
        	this.label3.Name = "label3";
        	this.label3.Size = new System.Drawing.Size(53, 12);
        	this.label3.TabIndex = 5;
        	this.label3.Text = "종료일 : ";
        	// 
        	// button_createDefault
        	// 
        	this.button_createDefault.Enabled = false;
        	this.button_createDefault.Location = new System.Drawing.Point(111, 391);
        	this.button_createDefault.Name = "button_createDefault";
        	this.button_createDefault.Size = new System.Drawing.Size(80, 23);
        	this.button_createDefault.TabIndex = 6;
        	this.button_createDefault.Text = "일지생성";
        	this.button_createDefault.UseVisualStyleBackColor = true;
        	this.button_createDefault.Click += new System.EventHandler(this.button_createDefault_Click);
        	// 
        	// button_createTotal
        	// 
        	this.button_createTotal.Enabled = false;
        	this.button_createTotal.Location = new System.Drawing.Point(197, 391);
        	this.button_createTotal.Name = "button_createTotal";
        	this.button_createTotal.Size = new System.Drawing.Size(80, 23);
        	this.button_createTotal.TabIndex = 7;
        	this.button_createTotal.Text = "합계생성";
        	this.button_createTotal.UseVisualStyleBackColor = true;
        	this.button_createTotal.Click += new System.EventHandler(this.button_createTotal_Click);
        	// 
        	// label_filepath
        	// 
        	this.label_filepath.AutoSize = true;
        	this.label_filepath.Font = new System.Drawing.Font("굴림", 7F);
        	this.label_filepath.Location = new System.Drawing.Point(14, 52);
        	this.label_filepath.Name = "label_filepath";
        	this.label_filepath.Size = new System.Drawing.Size(22, 10);
        	this.label_filepath.TabIndex = 8;
        	this.label_filepath.Text = "N/A";
        	// 
        	// menuStrip1
        	// 
        	this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.fileToolStripMenuItem,
			this.settingsToolStripMenuItem});
        	this.menuStrip1.Location = new System.Drawing.Point(0, 0);
        	this.menuStrip1.Name = "menuStrip1";
        	this.menuStrip1.Size = new System.Drawing.Size(298, 24);
        	this.menuStrip1.TabIndex = 9;
        	this.menuStrip1.Text = "menuStrip1";
        	// 
        	// fileToolStripMenuItem
        	// 
        	this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.openExcelToolStripMenuItem1,
			this.exitToolStripMenuItem});
        	this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
        	this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
        	this.fileToolStripMenuItem.Text = "File";
        	// 
        	// openExcelToolStripMenuItem1
        	// 
        	this.openExcelToolStripMenuItem1.Name = "openExcelToolStripMenuItem1";
        	this.openExcelToolStripMenuItem1.Size = new System.Drawing.Size(161, 22);
        	this.openExcelToolStripMenuItem1.Text = "Select-Excel-File";
        	this.openExcelToolStripMenuItem1.Click += new System.EventHandler(this.openExcelToolStripMenuItem_Click);
        	// 
        	// exitToolStripMenuItem
        	// 
        	this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
        	this.exitToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
        	this.exitToolStripMenuItem.Text = "Exit";
        	this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItemClick);
        	// 
        	// settingsToolStripMenuItem
        	// 
        	this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.settingsToolStripMenuItem1});
        	this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
        	this.settingsToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
        	this.settingsToolStripMenuItem.Text = "Settings";
        	// 
        	// settingsToolStripMenuItem1
        	// 
        	this.settingsToolStripMenuItem1.Name = "settingsToolStripMenuItem1";
        	this.settingsToolStripMenuItem1.Size = new System.Drawing.Size(117, 22);
        	this.settingsToolStripMenuItem1.Text = "Settings";
        	this.settingsToolStripMenuItem1.Click += new System.EventHandler(this.settingsToolStripMenuItem1_Click);
        	// 
        	// openFileDialog
        	// 
        	this.openFileDialog.Filter = "Excel Files|*.xlsx;*.xlsm;*.xlsb;*.xltx;*.xltm;*.xls;*.xlt;*.xls;*.xml;*.xml;*.xl" +
	"am;*.xla;*.xlw;*.xlr;";
        	this.openFileDialog.InitialDirectory = "./";
        	// 
        	// textBox_stateBox
        	// 
        	this.textBox_stateBox.Location = new System.Drawing.Point(14, 20);
        	this.textBox_stateBox.Multiline = true;
        	this.textBox_stateBox.Name = "textBox_stateBox";
        	this.textBox_stateBox.ReadOnly = true;
        	this.textBox_stateBox.Size = new System.Drawing.Size(246, 182);
        	this.textBox_stateBox.TabIndex = 0;
        	// 
        	// groupBox1
        	// 
        	this.groupBox1.Controls.Add(this.textBox_stateBox);
        	this.groupBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
        	this.groupBox1.Location = new System.Drawing.Point(11, 74);
        	this.groupBox1.Name = "groupBox1";
        	this.groupBox1.Size = new System.Drawing.Size(277, 220);
        	this.groupBox1.TabIndex = 0;
        	this.groupBox1.TabStop = false;
        	this.groupBox1.Text = "StateBox";
        	// 
        	// button_DB
        	// 
        	this.button_DB.Location = new System.Drawing.Point(25, 391);
        	this.button_DB.Name = "button_DB";
        	this.button_DB.Size = new System.Drawing.Size(80, 23);
        	this.button_DB.TabIndex = 10;
        	this.button_DB.Text = "DB연결";
        	this.button_DB.UseVisualStyleBackColor = true;
        	this.button_DB.Click += new System.EventHandler(this.button_DBClick);
        	// 
        	// Digital
        	// 
        	this.AllowDrop = true;
        	this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(298, 426);
        	this.Controls.Add(this.button_DB);
        	this.Controls.Add(this.label_filepath);
        	this.Controls.Add(this.button_createTotal);
        	this.Controls.Add(this.button_createDefault);
        	this.Controls.Add(this.label3);
        	this.Controls.Add(this.label2);
        	this.Controls.Add(this.dateTime_toDate);
        	this.Controls.Add(this.dateTime_fromDate);
        	this.Controls.Add(this.label1);
        	this.Controls.Add(this.groupBox1);
        	this.Controls.Add(this.menuStrip1);
        	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
        	this.MainMenuStrip = this.menuStrip1;
        	this.MaximizeBox = false;
        	this.Name = "Digital";
        	this.Text = "DigitalAuto";
        	this.Load += new System.EventHandler(this.Form1_Load);
        	this.menuStrip1.ResumeLayout(false);
        	this.menuStrip1.PerformLayout();
        	this.groupBox1.ResumeLayout(false);
        	this.groupBox1.PerformLayout();
        	this.ResumeLayout(false);
        	this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTime_fromDate;
        private System.Windows.Forms.DateTimePicker dateTime_toDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_createDefault;
        private System.Windows.Forms.Button button_createTotal;
        private System.Windows.Forms.Label label_filepath;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openExcelToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TextBox textBox_stateBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button_DB;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}

