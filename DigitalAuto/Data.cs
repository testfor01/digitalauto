﻿using System;
using System.Linq;

namespace DigitalAuto.Model.Data {
    public struct AgeGender {
		public AgeGender(
            uint man_young,
            uint woman_young,
            uint man_teen,
            uint woman_teen,
            uint man_adult,
            uint woman_adult
           ) : this(){
            this.man_young = man_young;
            this.woman_young = woman_young;
            this.man_teen = man_teen;
            this.woman_teen = woman_teen;
            this.man_adult = man_adult;
            this.woman_adult = woman_adult;
        }
        public uint man_young { get; set; }
        public uint woman_young { get; set; }
        public uint man_teen { get; set; }
        public uint woman_teen { get; set; }
        public uint man_adult { get; set; }
        public uint woman_adult { get; set; }
        public uint total(){
            return man_young + woman_young + man_teen + woman_teen + man_adult + woman_adult;
        }
    }
    public struct Corner {
        public Corner(
            uint dvd,
            uint laptop,
            uint pc,
            uint original 
        ) : this(){
            this.dvd = dvd;
            this.laptop = laptop;
            this.pc = pc;
            this.original = original;
        }
        public uint dvd { get; set; }
        public uint laptop { get; set; }
        public uint pc { get; set; }
        public uint original { get; set; }
        public uint total(){
            return dvd + laptop + pc + original;
        }
    }
}
