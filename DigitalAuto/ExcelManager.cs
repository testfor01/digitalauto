﻿using System;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Globalization;
using DigitalAuto.Model.Templete;
using DigitalAuto.Model.Data;
using System.Runtime.InteropServices;

namespace DigitalAuto {
    public class ExcelManager {

        private Application excelApp;
        private Workbook workbook, templete;
        private Worksheet defaultTempleteCopy, totalTempleteCopy;

        public enum KoreanDayOfWeek {
            일요일,
            월요일,
            화요일,
            수요일,
            목요일,
            금요일,
            토요일
        }

        public ExcelManager(string filePath, string templetePath, uint defaultSheetNo, uint totalSheetNo) {
            excelApp = new Application();
            try {
                workbook = excelApp.Workbooks.Open(Filename: filePath);
            }catch(COMException) {
                if(!File.Exists(filePath)) {
                    workbook = excelApp.Workbooks.Add();
                    workbook.SaveAs(Filename: filePath);
                } else {
                    throw new Exception("파일을 불러올 수 없습니다.");
                }
                
            }
            templete = excelApp.Workbooks.Open(Filename: templetePath);
            defaultTempleteCopy = templete.Worksheets.Item[defaultSheetNo];
            totalTempleteCopy = templete.Worksheets.Item[totalSheetNo];
        }

        // 탬플릿을 통해 복제하는 방식으로 변경
        public Worksheet CreateDefaultWorkSheet() {
            defaultTempleteCopy.Copy(Type.Missing,workbook.Sheets[workbook.Sheets.Count]);
            Worksheet ws = (Worksheet)workbook.Sheets[workbook.Sheets.Count];
            workbook.Save();
            return ws;
        }

        // 탬플릿을 통해 복제하는 방식으로 변경
        public Worksheet CreateTotalWorkSheet() {
            totalTempleteCopy.Copy(Type.Missing, workbook.Sheets[workbook.Sheets.Count]);
            Worksheet ws = (Worksheet)workbook.Sheets[workbook.Sheets.Count];
            workbook.Save();
            return ws;
        }

        public void BuildDefaultWorkSheet(Worksheet ws, ExcelTempleteObject templete, DateTime date, AgeGender ageGender, Corner corner){
            int errcount = 1;
            string sheetName = ToKoreanDateTime(date, templete.properties.format.default_form.dateSheetNameForm);
            try {
            	ws.Name = sheetName;
            } catch (COMException) {
                while (true) {
                    try {
                        errcount++;
                        ws.Name = date.ToString(String.Format("{0} ({1})",sheetName, errcount));
                        break;
                    } catch (COMException) { }
                }
            }
            ws.Range[templete.properties.format.default_form.date].Value2 = ToKoreanDateTime(date, templete.properties.format.default_form.dateForm);
            ws.Range[templete.properties.format.default_form.age_gender.man_young].Value2 = ageGender.man_young;
            ws.Range[templete.properties.format.default_form.age_gender.man_teen].Value2 = ageGender.man_teen;
            ws.Range[templete.properties.format.default_form.age_gender.man_adult].Value2 = ageGender.man_adult;
            ws.Range[templete.properties.format.default_form.age_gender.woman_young].Value2 = ageGender.woman_young;
            ws.Range[templete.properties.format.default_form.age_gender.woman_teen].Value2 = ageGender.woman_teen;
            ws.Range[templete.properties.format.default_form.age_gender.woman_adult].Value2 = ageGender.woman_adult;
            ws.Range[templete.properties.format.default_form.corner.dvd].Value2 = corner.dvd;
            ws.Range[templete.properties.format.default_form.corner.laptop].Value2 = corner.laptop;
            ws.Range[templete.properties.format.default_form.corner.pc].Value2 = corner.pc;
            ws.Range[templete.properties.format.default_form.corner.original].Value2 = corner.original;
            workbook.Save();
        }

        public void BuildTotalWorkSheet(Worksheet ws, ExcelTempleteObject templete, DateTime start, DateTime end){
            int errcount = 1;
            string sheetName = templete.properties.format.total_form.sheetName;
            try {
            	ws.Name = sheetName;
            } catch (COMException) {
                while (true) {
                    try {
                        errcount++;
                        ws.Name = String.Format("{0} ({1})",sheetName,errcount);
                        break;
                    } catch (COMException) { }
                }
            }
            ws.Range[templete.properties.format.total_form.fromDate].Value2 = ToKoreanDateTime(start, templete.properties.format.total_form.fromDateForm);
            ws.Range[templete.properties.format.total_form.toDate].Value2 = ToKoreanDateTime(end, templete.properties.format.total_form.toDateForm);
            string fromdateSheet = start.ToString(templete.properties.format.default_form.dateSheetNameForm);
            string todateSheet = end.ToString(templete.properties.format.default_form.dateSheetNameForm);
            ws.Range[templete.properties.format.total_form.age_gender.man_young].Value2 = String.Format("=SUM('{0}:{1}'!{2})",fromdateSheet,todateSheet,templete.properties.format.default_form.age_gender.man_young);
            ws.Range[templete.properties.format.total_form.age_gender.man_teen].Value2 = String.Format("=SUM('{0}:{1}'!{2})",fromdateSheet,todateSheet,templete.properties.format.default_form.age_gender.man_teen);
            ws.Range[templete.properties.format.total_form.age_gender.man_adult].Value2 = String.Format("=SUM('{0}:{1}'!{2})",fromdateSheet,todateSheet,templete.properties.format.default_form.age_gender.man_adult);
            ws.Range[templete.properties.format.total_form.age_gender.woman_young].Value2 = String.Format("=SUM('{0}:{1}'!{2})",fromdateSheet,todateSheet,templete.properties.format.default_form.age_gender.woman_young);
            ws.Range[templete.properties.format.total_form.age_gender.woman_teen].Value2 = String.Format("=SUM('{0}:{1}'!{2})",fromdateSheet,todateSheet,templete.properties.format.default_form.age_gender.woman_teen);
            ws.Range[templete.properties.format.total_form.age_gender.woman_adult].Value2 = String.Format("=SUM('{0}:{1}'!{2})",fromdateSheet,todateSheet,templete.properties.format.default_form.age_gender.woman_adult);
            ws.Range[templete.properties.format.total_form.corner.dvd].Value2 = String.Format("=SUM('{0}:{1}'!{2})",fromdateSheet,todateSheet,templete.properties.format.default_form.corner.dvd);
            ws.Range[templete.properties.format.total_form.corner.laptop].Value2 = String.Format("=SUM('{0}:{1}'!{2})",fromdateSheet,todateSheet,templete.properties.format.default_form.corner.laptop);
            ws.Range[templete.properties.format.total_form.corner.pc].Value2 = String.Format("=SUM('{0}:{1}'!{2})",fromdateSheet,todateSheet,templete.properties.format.default_form.corner.pc);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
            ws.Range[templete.properties.format.total_form.corner.original].Value2 = String.Format("=SUM('{0}:{1}'!{2})",fromdateSheet,todateSheet,templete.properties.format.default_form.corner.original);
            workbook.Save();
        }

        public void InsertData(Worksheet ws, string pos, string data) {
            ws.Range[pos].Value2 = data;
            workbook.Save();
        }

        public static String ToKoreanDateTime(DateTime date, string format) {
            return date.ToString(format, CultureInfo.CreateSpecificCulture("ko-KR"));
        }

        public void Close(){
            workbook.Save();
            workbook.Close();
            templete.Close();
        }

    }
}
