﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace DigitalAuto {
    public partial class Form2 : Form {
        public Form2() {
            InitializeComponent();
            Properties.Settings.Default.Reload();
            this.textBox_2Ip.Text = Properties.Settings.Default.ip;
            this.textBox_2LibraryCode.Text = Properties.Settings.Default.libraryCode;
            this.textBox_2Password.Text = Properties.Settings.Default.dbPassword;
            this.textBox_2Port.Text = Properties.Settings.Default.port.ToString();
            this.textBox_2SID.Text = Properties.Settings.Default.dbSID;
            this.textBox_2UserId.Text = Properties.Settings.Default.dbUserId;
            this.checkBox_2AbsentDaySkip.Checked = Properties.Settings.Default.isExceptNobodyDay;
            this.checkBox_2ExecuteAfterProcess.Checked = Properties.Settings.Default.executeAfterProcess;
        }

        private void button_2OK_Click(object sender, EventArgs e) {
			saveSettings();
        }
		
		private void saveSettings(){
			try {
                Properties.Settings.Default.ip = this.textBox_2Ip.Text;
                Properties.Settings.Default.libraryCode = this.textBox_2LibraryCode.Text;
                Properties.Settings.Default.dbPassword = this.textBox_2Password.Text;
                Properties.Settings.Default.port = uint.Parse(this.textBox_2Port.Text);
                Properties.Settings.Default.dbSID = this.textBox_2SID.Text;
                Properties.Settings.Default.dbUserId = this.textBox_2UserId.Text;
                Properties.Settings.Default.isExceptNobodyDay = this.checkBox_2AbsentDaySkip.Checked;
                Properties.Settings.Default.executeAfterProcess = this.checkBox_2ExecuteAfterProcess.Checked;
                MessageBox.Show("완료");
                Properties.Settings.Default.Save();
                this.Close();
            } catch (Exception) {
                MessageBox.Show("변경실패, 유효하지 않은 설정.");
            }
		}

        private void button_2Cancel_Click(object sender, EventArgs e) {
            Close();
        }
    }
}
