﻿namespace DigitalAuto {
    partial class Form2 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
        	this.groupBox1 = new System.Windows.Forms.GroupBox();
        	this.label4 = new System.Windows.Forms.Label();
        	this.label3 = new System.Windows.Forms.Label();
        	this.label2 = new System.Windows.Forms.Label();
        	this.label1 = new System.Windows.Forms.Label();
        	this.textBox_2Port = new System.Windows.Forms.TextBox();
        	this.textBox_2Ip = new System.Windows.Forms.TextBox();
        	this.textBox_2Password = new System.Windows.Forms.TextBox();
        	this.textBox_2SID = new System.Windows.Forms.TextBox();
        	this.textBox_2UserId = new System.Windows.Forms.TextBox();
        	this.button_2OK = new System.Windows.Forms.Button();
        	this.button_2Cancel = new System.Windows.Forms.Button();
        	this.groupBox2 = new System.Windows.Forms.GroupBox();
        	this.checkBox_2AbsentDaySkip = new System.Windows.Forms.CheckBox();
        	this.textBox_2LibraryCode = new System.Windows.Forms.TextBox();
        	this.label5 = new System.Windows.Forms.Label();
        	this.checkBox_2ExecuteAfterProcess = new System.Windows.Forms.CheckBox();
        	this.groupBox1.SuspendLayout();
        	this.groupBox2.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// groupBox1
        	// 
        	this.groupBox1.Controls.Add(this.label4);
        	this.groupBox1.Controls.Add(this.label3);
        	this.groupBox1.Controls.Add(this.label2);
        	this.groupBox1.Controls.Add(this.label1);
        	this.groupBox1.Controls.Add(this.textBox_2Port);
        	this.groupBox1.Controls.Add(this.textBox_2Ip);
        	this.groupBox1.Controls.Add(this.textBox_2Password);
        	this.groupBox1.Controls.Add(this.textBox_2SID);
        	this.groupBox1.Controls.Add(this.textBox_2UserId);
        	this.groupBox1.Location = new System.Drawing.Point(9, 12);
        	this.groupBox1.Name = "groupBox1";
        	this.groupBox1.Size = new System.Drawing.Size(257, 135);
        	this.groupBox1.TabIndex = 0;
        	this.groupBox1.TabStop = false;
        	this.groupBox1.Text = "Database";
        	// 
        	// label4
        	// 
        	this.label4.AutoSize = true;
        	this.label4.Location = new System.Drawing.Point(17, 98);
        	this.label4.Name = "label4";
        	this.label4.Size = new System.Drawing.Size(62, 12);
        	this.label4.TabIndex = 8;
        	this.label4.Text = "Password";
        	// 
        	// label3
        	// 
        	this.label3.AutoSize = true;
        	this.label3.Location = new System.Drawing.Point(6, 61);
        	this.label3.Name = "label3";
        	this.label3.Size = new System.Drawing.Size(78, 12);
        	this.label3.TabIndex = 7;
        	this.label3.Text = "User Id / SID";
        	// 
        	// label2
        	// 
        	this.label2.AutoSize = true;
        	this.label2.Location = new System.Drawing.Point(154, 23);
        	this.label2.Name = "label2";
        	this.label2.Size = new System.Drawing.Size(27, 12);
        	this.label2.TabIndex = 6;
        	this.label2.Text = "Port";
        	// 
        	// label1
        	// 
        	this.label1.AutoSize = true;
        	this.label1.Location = new System.Drawing.Point(17, 23);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(16, 12);
        	this.label1.TabIndex = 5;
        	this.label1.Text = "IP";
        	// 
        	// textBox_2Port
        	// 
        	this.textBox_2Port.Location = new System.Drawing.Point(196, 20);
        	this.textBox_2Port.MaxLength = 5;
        	this.textBox_2Port.Name = "textBox_2Port";
        	this.textBox_2Port.Size = new System.Drawing.Size(49, 21);
        	this.textBox_2Port.TabIndex = 4;
        	// 
        	// textBox_2Ip
        	// 
        	this.textBox_2Ip.ImeMode = System.Windows.Forms.ImeMode.Off;
        	this.textBox_2Ip.Location = new System.Drawing.Point(39, 20);
        	this.textBox_2Ip.Name = "textBox_2Ip";
        	this.textBox_2Ip.Size = new System.Drawing.Size(100, 21);
        	this.textBox_2Ip.TabIndex = 3;
        	// 
        	// textBox_2Password
        	// 
        	this.textBox_2Password.ImeMode = System.Windows.Forms.ImeMode.Disable;
        	this.textBox_2Password.Location = new System.Drawing.Point(90, 95);
        	this.textBox_2Password.Name = "textBox_2Password";
        	this.textBox_2Password.PasswordChar = '●';
        	this.textBox_2Password.Size = new System.Drawing.Size(157, 21);
        	this.textBox_2Password.TabIndex = 2;
        	// 
        	// textBox_2SID
        	// 
        	this.textBox_2SID.ImeMode = System.Windows.Forms.ImeMode.Off;
        	this.textBox_2SID.Location = new System.Drawing.Point(196, 58);
        	this.textBox_2SID.Name = "textBox_2SID";
        	this.textBox_2SID.Size = new System.Drawing.Size(51, 21);
        	this.textBox_2SID.TabIndex = 1;
        	// 
        	// textBox_2UserId
        	// 
        	this.textBox_2UserId.ImeMode = System.Windows.Forms.ImeMode.Off;
        	this.textBox_2UserId.Location = new System.Drawing.Point(90, 58);
        	this.textBox_2UserId.Name = "textBox_2UserId";
        	this.textBox_2UserId.Size = new System.Drawing.Size(100, 21);
        	this.textBox_2UserId.TabIndex = 0;
        	// 
        	// button_2OK
        	// 
        	this.button_2OK.Location = new System.Drawing.Point(41, 298);
        	this.button_2OK.Name = "button_2OK";
        	this.button_2OK.Size = new System.Drawing.Size(75, 23);
        	this.button_2OK.TabIndex = 1;
        	this.button_2OK.Text = "OK";
        	this.button_2OK.UseVisualStyleBackColor = true;
        	this.button_2OK.Click += new System.EventHandler(this.button_2OK_Click);
        	// 
        	// button_2Cancel
        	// 
        	this.button_2Cancel.Location = new System.Drawing.Point(156, 298);
        	this.button_2Cancel.Name = "button_2Cancel";
        	this.button_2Cancel.Size = new System.Drawing.Size(75, 23);
        	this.button_2Cancel.TabIndex = 2;
        	this.button_2Cancel.Text = "Cancel";
        	this.button_2Cancel.UseVisualStyleBackColor = true;
        	this.button_2Cancel.Click += new System.EventHandler(this.button_2Cancel_Click);
        	// 
        	// groupBox2
        	// 
        	this.groupBox2.Controls.Add(this.checkBox_2ExecuteAfterProcess);
        	this.groupBox2.Controls.Add(this.checkBox_2AbsentDaySkip);
        	this.groupBox2.Controls.Add(this.textBox_2LibraryCode);
        	this.groupBox2.Controls.Add(this.label5);
        	this.groupBox2.Location = new System.Drawing.Point(9, 162);
        	this.groupBox2.Name = "groupBox2";
        	this.groupBox2.Size = new System.Drawing.Size(256, 104);
        	this.groupBox2.TabIndex = 3;
        	this.groupBox2.TabStop = false;
        	this.groupBox2.Text = "Etc";
        	// 
        	// checkBox_2AbsentDaySkip
        	// 
        	this.checkBox_2AbsentDaySkip.AutoSize = true;
        	this.checkBox_2AbsentDaySkip.Location = new System.Drawing.Point(66, 55);
        	this.checkBox_2AbsentDaySkip.Name = "checkBox_2AbsentDaySkip";
        	this.checkBox_2AbsentDaySkip.Size = new System.Drawing.Size(115, 16);
        	this.checkBox_2AbsentDaySkip.TabIndex = 3;
        	this.checkBox_2AbsentDaySkip.Text = "AbsentDaySkip?";
        	this.checkBox_2AbsentDaySkip.UseVisualStyleBackColor = true;
        	// 
        	// textBox_2LibraryCode
        	// 
        	this.textBox_2LibraryCode.ImeMode = System.Windows.Forms.ImeMode.Off;
        	this.textBox_2LibraryCode.Location = new System.Drawing.Point(122, 25);
        	this.textBox_2LibraryCode.Name = "textBox_2LibraryCode";
        	this.textBox_2LibraryCode.Size = new System.Drawing.Size(100, 21);
        	this.textBox_2LibraryCode.TabIndex = 1;
        	// 
        	// label5
        	// 
        	this.label5.AutoSize = true;
        	this.label5.Location = new System.Drawing.Point(29, 28);
        	this.label5.Name = "label5";
        	this.label5.Size = new System.Drawing.Size(78, 12);
        	this.label5.TabIndex = 0;
        	this.label5.Text = "Library Code";
        	// 
        	// checkBox_2ExecuteAfterProcess
        	// 
        	this.checkBox_2ExecuteAfterProcess.AutoSize = true;
        	this.checkBox_2ExecuteAfterProcess.Location = new System.Drawing.Point(66, 77);
        	this.checkBox_2ExecuteAfterProcess.Name = "checkBox_2ExecuteAfterProcess";
        	this.checkBox_2ExecuteAfterProcess.Size = new System.Drawing.Size(148, 16);
        	this.checkBox_2ExecuteAfterProcess.TabIndex = 4;
        	this.checkBox_2ExecuteAfterProcess.Text = "ExecuteAfterProcess?";
        	this.checkBox_2ExecuteAfterProcess.UseVisualStyleBackColor = true;
        	// 
        	// Form2
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(274, 333);
        	this.Controls.Add(this.groupBox2);
        	this.Controls.Add(this.button_2Cancel);
        	this.Controls.Add(this.button_2OK);
        	this.Controls.Add(this.groupBox1);
        	this.DoubleBuffered = true;
        	this.MaximizeBox = false;
        	this.MinimizeBox = false;
        	this.Name = "Form2";
        	this.Text = "Settings";
        	this.groupBox1.ResumeLayout(false);
        	this.groupBox1.PerformLayout();
        	this.groupBox2.ResumeLayout(false);
        	this.groupBox2.PerformLayout();
        	this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_2Port;
        private System.Windows.Forms.TextBox textBox_2Ip;
        private System.Windows.Forms.TextBox textBox_2Password;
        private System.Windows.Forms.TextBox textBox_2SID;
        private System.Windows.Forms.TextBox textBox_2UserId;
        private System.Windows.Forms.Button button_2OK;
        private System.Windows.Forms.Button button_2Cancel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBox_2AbsentDaySkip;
        private System.Windows.Forms.TextBox textBox_2LibraryCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox_2ExecuteAfterProcess;
    }
}