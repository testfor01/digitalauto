﻿using System;
using System.Linq;

namespace DigitalAuto.Model.Templete {
    public struct AgeGenderAddress {
        public string man_young { get; set; }
        public string woman_young { get; set; }
        public string man_teen { get; set; }
        public string woman_teen { get; set; }
        public string man_adult { get; set; }
        public string woman_adult { get; set; }
    }
    public struct CornerAddress {
        public string dvd { get; set; }
        public string laptop { get; set; }
        public string pc { get; set; }
        public string original { get; set; }
    }
}
