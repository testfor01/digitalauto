﻿using DigitalAuto.Model;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace DigitalAuto {
    public partial class Digital : Form
    {
    	
    	DigitalAutoExecutor digital = null;
    	
        public Digital()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            Initialize();
        }

        private void Initialize() {
            Properties.Settings.Default.Reload();
            this.label_filepath.Text = Properties.Settings.Default.target_file;
            this.openFileDialog.FileName = Properties.Settings.Default.target_file;
            this.dateTime_fromDate.Value = Properties.Settings.Default.startDate;
            this.dateTime_toDate.Value = Properties.Settings.Default.endDate;
        }

        //메뉴->파일선택 클릭
        void openExcelToolStripMenuItem_Click(object sender, EventArgs e) {
        	setExcelFile();
        }
        
        //메뉴->종료클릭
		void ExitToolStripMenuItemClick(object sender, EventArgs e) {
			Close();
		}        

        //메뉴->설정선택 클릭
        void settingsToolStripMenuItem1_Click(object sender, EventArgs e) {
        	openSettings();
        }
        
        //DB버튼 클릭
       	void button_DBClick(object sender, EventArgs e) {
        	createOrCloseDB();
		}
        
        //일지생성 클릭
        void button_createDefault_Click(object sender, EventArgs e) {
        	createDefault();
        }

        //통합생성 클릭
        void button_createTotal_Click(object sender, EventArgs e) {
        	createTotal();
        }
        
       
		private void Append(string text) {
            this.textBox_stateBox.AppendText(text + "\n");
        }
        
        private void setExcelFile(){
            DialogResult result = this.openFileDialog.ShowDialog();
            if(result == DialogResult.OK) {
                Properties.Settings.Default.target_file = this.openFileDialog.FileName;
                Properties.Settings.Default.Save();
                Append("신규파일등록 : " + this.openFileDialog.FileName);
                Append("");
                Initialize();
            }
        }
        
        private void openSettings(){
			Form2 form2 = new Form2();
            form2.Show();
        }
        
        private void createOrCloseDB(){
        	if(digital == null){
        		try {
	        		string ip = Properties.Settings.Default.ip;
	                uint port = Properties.Settings.Default.port;
	                string userId = Properties.Settings.Default.dbUserId;
	                string password = Properties.Settings.Default.dbPassword;
	                string sid = Properties.Settings.Default.dbSID;
	                string templete = Properties.Settings.Default.templete;
	        		digital = new DigitalAutoExecutor(ip, port, userId, password, sid, templete);
	        		Append("##DB연결완료##");
	        		Append("");
	        		this.button_DB.Text = "DB연결해제";
	        		this.button_createDefault.Enabled = true;
	        		this.button_createTotal.Enabled = true;
        		} catch(Exception ex) {
        			errorHandle(ex);
        		}
        	}else{
        		digital.Close();
        		digital = null;
        		Append("##DB연결해제완료##");
        		Append("");
        		this.button_DB.Text = "DB연결";
        		this.button_createDefault.Enabled = false;
        		this.button_createTotal.Enabled = false;
        	}
        }
        private void createDefault(){
            try {
                string target_file = Properties.Settings.Default.target_file;
                DateTime fromDate = this.dateTime_fromDate.Value;
                DateTime toDate = this.dateTime_toDate.Value;
                bool isAbsentSkip = Properties.Settings.Default.isExceptNobodyDay;
                string libraryCode = Properties.Settings.Default.libraryCode;
                uint gap = Properties.Settings.Default.maximumGap;
                bool isExecuted = Properties.Settings.Default.executeAfterProcess;

                Append("##일지생성시작##");
                Append("파일 : " + Properties.Settings.Default.target_file);
                Append("시작일 : " + fromDate.ToString("yyyy년 M월 d일"));
                Append("종료일 : " + toDate.ToString("yyyy년 M월 d일"));
                Append("");

                digital.StatisticRun(target_file, fromDate, toDate, libraryCode, isAbsentSkip, gap);

                Append("##생성끝##");
                Append("");
                if(isExecuted){
                	Process.Start(target_file);
                }
            } catch(Exception ex) {
        		errorHandle(ex);
            }
        }
        
        private void createTotal(){
            try {
                string target_file = Properties.Settings.Default.target_file;
                bool isExecuted = Properties.Settings.Default.executeAfterProcess;
                DateTime fromDate = this.dateTime_fromDate.Value;
                DateTime toDate = this.dateTime_toDate.Value;

                Append("##합계생성시작##");
                Append("파일 : " + Properties.Settings.Default.target_file);
                Append("시작일 : " + fromDate.ToString("yyyy년 M월 d일"));
                Append("종료일 : " + toDate.ToString("yyyy년 M월 d일"));
                Append("");

                digital.SummaryRun(target_file, fromDate, toDate);

                Append("##생성끝##");
                Append("");
                if(isExecuted){
                	Process.Start(target_file);
                }
            } catch (Exception ex) {
        		errorHandle(ex);
            }
        }

        private void dateTime_fromDate_ValueChanged(object sender, EventArgs e) {
            Properties.Settings.Default.startDate = this.dateTime_fromDate.Value;
            Properties.Settings.Default.Save();
        }

        private void dateTime_toDate_ValueChanged(object sender, EventArgs e) {
            Properties.Settings.Default.endDate = this.dateTime_toDate.Value;
            Properties.Settings.Default.Save();
        }
        
        private void errorHandle(Exception ex){
    	    if (digital != null) {
                digital.Close();
            }
            Append("##에러발생##");
            Append("");
            Append(ex.Message);
            Append("");
        }
		


    }
}
