﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using DigitalAuto.Model.DAO;
using DigitalAuto.Model.Templete;
using DigitalAuto.Model.Data;
using Newtonsoft.Json;

namespace DigitalAuto.Model {
    public class DigitalAutoExecutor {

        readonly OracleDBManager dbManager;
        ExcelTempleteObject templete;

        public DigitalAutoExecutor(string ip, uint port, string dbUserId, string dbPassword, string dbSID, string templeteJsonPath) {
            dbManager = new OracleDBManager(ip, port);
            dbManager.ConnectionDB(dbUserId, dbSID, dbPassword);
            JsonSerializer serializer = new JsonSerializer();
            JsonReader reader = new JsonTextReader(new StreamReader(templeteJsonPath));
            templete = serializer.Deserialize<ExcelTempleteObject>(reader);
        }

        private void DistributionValue(ref uint a, ref uint b, ref uint v){
            if(v == 0) return;
            if(a >= b){
                double p;
                if(a+b == 0){
                    p = 0.5;
                }else
                    p = b / (a+b);
                uint Q = (uint)p*v;
                b += Q;
                v -= Q;
                a += v;
                v = 0;
            }else{
                DistributionValue(ref b, ref a, ref v);
            }
        }

        public AgeGender BuildAgeGender(DateTime date, string libraryCode) {
            List<String[]> result1 = dbManager.Query(QueryForm.GetAgeGenderQueryString(QueryForm.Gender.남자, date, libraryCode));
            List<String[]> result2 = dbManager.Query(QueryForm.GetAgeGenderQueryString(QueryForm.Gender.여자, date, libraryCode));
            List<String[]> result3 = dbManager.Query(QueryForm.GetAgeGenderQueryString(QueryForm.Gender.알수없음, date, libraryCode));
            Dictionary<QueryForm.Age, UInt32> manResult = new Dictionary<QueryForm.Age, UInt32>();
            Dictionary<QueryForm.Age, UInt32> womanResult = new Dictionary<QueryForm.Age, UInt32>();
            Dictionary<QueryForm.Age, UInt32> unknownResult = new Dictionary<QueryForm.Age, UInt32>();
            foreach (String[] strs in result1) {
                manResult.Add((QueryForm.Age)uint.Parse(strs[0]), uint.Parse(strs[1]));
            }
            foreach (String[] strs in result2) {
                womanResult.Add((QueryForm.Age)uint.Parse(strs[0]), uint.Parse(strs[1]));
            }
            foreach (String[] strs in result3) {
                unknownResult.Add((QueryForm.Age)uint.Parse(strs[0]), uint.Parse(strs[1]));
            }
            uint man_young = manResult[QueryForm.Age.영유아] + manResult[QueryForm.Age.초등학생];
            uint woman_young = womanResult[QueryForm.Age.영유아] + womanResult[QueryForm.Age.초등학생];
            uint unknown_young = unknownResult[QueryForm.Age.영유아] + unknownResult[QueryForm.Age.초등학생];
            DistributionValue(ref man_young, ref woman_young, ref unknown_young);

            uint man_teen = manResult[QueryForm.Age.중학생] + manResult[QueryForm.Age.고등학생];
            uint woman_teen = womanResult[QueryForm.Age.중학생] + womanResult[QueryForm.Age.고등학생];
            uint unknown_teen = unknownResult[QueryForm.Age.중학생] + unknownResult[QueryForm.Age.고등학생];
            DistributionValue(ref man_teen, ref woman_teen, ref unknown_teen);

            uint man_adult = manResult[QueryForm.Age.일반] + manResult[QueryForm.Age.실버];
            uint woman_adult = womanResult[QueryForm.Age.일반] + womanResult[QueryForm.Age.실버];
            uint unknown_adult = unknownResult[QueryForm.Age.일반] + unknownResult[QueryForm.Age.실버];
            DistributionValue(ref man_adult, ref woman_adult, ref unknown_adult);

            uint man_unknown = manResult[QueryForm.Age.알수없음];
            uint woman_unknown = womanResult[QueryForm.Age.알수없음];
            uint unknown_unknown = unknownResult[QueryForm.Age.알수없음];
            DistributionValue(ref man_unknown, ref woman_unknown, ref unknown_unknown);
            DistributionValue(ref man_teen, ref man_adult, ref man_unknown);
            DistributionValue(ref woman_teen, ref woman_adult, ref woman_unknown);
            return new AgeGender(man_young, woman_young, man_teen, woman_teen, man_adult, woman_adult);
        }

        public Data.Corner BuildCorner(DateTime date, string libraryCode) {
            uint pc = uint.Parse(dbManager.Query(QueryForm.GetCornerQueryString(QueryForm.Corner.인터넷, date, libraryCode)).ElementAt(0).ElementAt(1));
            uint laptop = uint.Parse(dbManager.Query(QueryForm.GetCornerQueryString(QueryForm.Corner.노트북, date, libraryCode)).ElementAt(0).ElementAt(1));
            uint dvd = uint.Parse(dbManager.Query(QueryForm.GetCornerQueryString(QueryForm.Corner.DVD상영실, date, libraryCode)).ElementAt(0).ElementAt(1));
            uint original = uint.Parse(dbManager.Query(QueryForm.GetCornerQueryString(QueryForm.Corner.원문검색, date, libraryCode)).ElementAt(0).ElementAt(1));
            return new Data.Corner(dvd,laptop,pc,original);
        }

        private void BuildDefaultForm(ExcelManager excelManager, DateTime fromDate, DateTime toDate, string libraryCode, bool isExceptNobodyDay) {
            int gap = (toDate - fromDate).Days;
            for (int i = 0; i < gap + 1; i++) {
                DateTime targetTime = fromDate.AddDays(i);
                AgeGender ageGender = BuildAgeGender(targetTime,libraryCode);
                Data.Corner corner = BuildCorner(targetTime,libraryCode);
                if(isExceptNobodyDay && ageGender.total() == 0 && corner.total() == 0){
                    continue; 
                }else{
                    excelManager.BuildDefaultWorkSheet(excelManager.CreateDefaultWorkSheet(), templete, targetTime, ageGender, corner);
                }
            }
        }

        private void BuildTotalForm(ExcelManager excelManager, DateTime fromDate, DateTime toDate) {
            excelManager.BuildTotalWorkSheet(excelManager.CreateTotalWorkSheet(), templete, fromDate, toDate);
        }

        public void SummaryRun(string targetFile, DateTime fromDate, DateTime toDate) {
            ExcelManager excelManager = new ExcelManager(targetFile, templete.properties.style.file, templete.properties.style.defaultSheetNo, templete.properties.style.totalSheetNo);
            BuildTotalForm(excelManager, fromDate, toDate);
            excelManager.Close();
        }

        public void StatisticRun(string target_file, DateTime fromDate, DateTime toDate, string libraryCode, bool isExceptNobodyDay, uint maximumGap) {
            ExcelManager excelManager = new ExcelManager(target_file, templete.properties.style.file, templete.properties.style.defaultSheetNo, templete.properties.style.totalSheetNo);
            int gap = (toDate - fromDate).Days;
            if (gap < 0 || gap > maximumGap) {
                throw new Exception("유효하지 않은 시간너비");
            }
            BuildDefaultForm(excelManager, fromDate, toDate, libraryCode, isExceptNobodyDay);
            excelManager.Close();
        }

        public void Close(){
            dbManager.Close();
        }


    }
}
