﻿using System;
using System.Linq;
using DigitalAuto.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DigitalAuto.Model.Tests {
    [TestClass()]
    public class QueryFormTests {
        [TestMethod()]
        public void GetAgeGenderQueryStringTest() {
            Assert.AreEqual(
                @"
                SELECT 
                    ROW_ALIAS.AGE_SUB_LIST,
                    COUNT(AGE_SUB)
                FROM
                    (
                    SELECT
                        TRUNC(DSV.START_TIME, 'DD') AS START_TIME,
                        KCT.CODE_ALIAS AS CORNER,
                        SPT.CAPTION AS SEAT_NUM,
                        TO_CHAR(DSV.START_TIME, 'HH24') AS TIME_SLOT,
                        TRUNC(GET_ELAPSED_TIME(DSV.START_TIME, NVL(DSV.STOP_TIME, DSV.STOP_PLAN_TIME)), -1) AS USE_TIME,
                        NVL(CLU.GPIN_SEX, 2) AS GENDER,
                        TO_CHAR(DSV.START_TIME, 'd') AS DAY,
                        CASE
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1) < 10 THEN '0'
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1) BETWEEN 10 AND 19 THEN '1'
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1) BETWEEN 20 AND 29 THEN '2'
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1) BETWEEN 30 AND 39 THEN '3'
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1) BETWEEN 40 AND 49 THEN '4'
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1) BETWEEN 50 AND 59 THEN '5'
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1) BETWEEN 60 AND 69 THEN '6'
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1) >= 70 THEN '7'
                                ELSE '8'
                            END AS AGE,
                        CASE
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY')) BETWEEN 0 AND 6 THEN '0'
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY')) BETWEEN 7 AND 12 THEN '1'
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY')) BETWEEN 13 AND 15 THEN '2'
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY')) BETWEEN 16 AND 18 THEN '3'
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY')) BETWEEN 19 AND 64 THEN '4'
                            WHEN(TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY')) >= 65 THEN '5'
                                ELSE '6'
                            END AS AGE_SUB
                    FROM
                        KSM_DIGI_SEAT_VIEW DSV,
                        KSM_SEAT_POS_TBL SPT,
                        KSM_CORNER_TBL KCT,
                        CO_LOAN_USER_TBL CLU,
                        KSM_ROOM_TBL KRT
                    WHERE
                        DSV.STATUS IN('0', '1', '5') AND
                        DSV.MANAGE_CODE = 'MA' AND
                        KRT.MANAGE_CODE = 'MA' AND
                        KCT.MANAGE_CODE = 'MA' AND
                        KCT.ROOM_TYPE = 'D' AND
                        DSV.SEAT_KEY = SPT.REC_KEY AND
                        SPT.CORNER_CODE = KCT.CODE      AND
                        DSV.USER_KEY = CLU.REC_KEY(+)   AND
                        SPT.ROOM_CODE = KRT.CODE AND
                        DSV.START_TIME BETWEEN(TO_DATE('20170325', 'YYYY/MM/DD')) AND
                        (TO_DATE('20170325', 'YYYY/MM/DD') + 1)   AND
                        NVL(CLU.GPIN_SEX, 2) = '0'    AND
                        KRT.CODE IN('001')    AND
                        KCT.CODE IN('601', '600', '603', '602')) BODY,
                        (
                            SELECT
                                0 + LEVEL - 1 AS AGE_SUB_LIST
                            FROM
                                dual CONNECT BY 0 + LEVEL - 1 <= 6
                        ) ROW_ALIAS
                    WHERE
                        ROW_ALIAS.AGE_SUB_LIST = AGE_SUB(+)
                    GROUP BY
                        ROW_ALIAS.AGE_SUB_LIST
                    ORDER BY ROW_ALIAS.AGE_SUB_LIST
                ",
            QueryForm.GetAgeGenderQueryString(QueryForm.Gender.남자, new DateTime(2017, 3, 25), "MA"));
        }

        [TestMethod()]
        public void GetCornerQueryStringTest() {
            Assert.AreEqual(
                @"SELECT
                  TO_CHAR(ROW_ALIAS.DAY_LIST, 'YYYY/MM/DD'),
                  COUNT(START_TIME)
                FROM
                  (
                    SELECT
                      TRUNC(DSV.START_TIME, 'DD') AS START_TIME,
                      KCT.CODE_ALIAS AS CORNER,
                      SPT.CAPTION AS SEAT_NUM,
                      TO_CHAR(DSV.START_TIME, 'HH24') AS TIME_SLOT,
                      TRUNC(
                        GET_ELAPSED_TIME(
                          DSV.START_TIME,
                          NVL(DSV.STOP_TIME, DSV.STOP_PLAN_TIME)
                        ),
                        -1
                      ) AS USE_TIME,
                      NVL(CLU.GPIN_SEX, 2) AS GENDER,
                      TO_CHAR(DSV.START_TIME, 'd') AS DAY,
                      CASE WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1
                      ) < 10 THEN '0' WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1
                      ) BETWEEN 10
                      AND 19 THEN '1' WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1
                      ) BETWEEN 20
                      AND 29 THEN '2' WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1
                      ) BETWEEN 30
                      AND 39 THEN '3' WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1
                      ) BETWEEN 40
                      AND 49 THEN '4' WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1
                      ) BETWEEN 50
                      AND 59 THEN '5' WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1
                      ) BETWEEN 60
                      AND 69 THEN '6' WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY') + 1
                      ) >= 70 THEN '7' ELSE '8' END AS AGE,
                      CASE WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY')
                      ) BETWEEN 0
                      AND 6 THEN '0' WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY')
                      ) BETWEEN 7
                      AND 12 THEN '1' WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY')
                      ) BETWEEN 13
                      AND 15 THEN '2' WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY')
                      ) BETWEEN 16
                      AND 18 THEN '3' WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY')
                      ) BETWEEN 19
                      AND 64 THEN '4' WHEN(
                        TO_CHAR(SYSDATE, 'YYYY') - TO_CHAR(CLU.BIRTHDAY, 'YYYY')
                      ) >= 65 THEN '5' ELSE '6' END AS AGE_SUB
                    FROM
                      KSM_DIGI_SEAT_VIEW DSV,
                      KSM_SEAT_POS_TBL SPT,
                      KSM_CORNER_TBL KCT,
                      CO_LOAN_USER_TBL CLU,
                      KSM_ROOM_TBL KRT
                    WHERE
                      DSV.STATUS IN('0', '1', '5')
                      AND DSV.MANAGE_CODE = 'MA'
                      AND KRT.MANAGE_CODE = 'MA'
                      AND KCT.MANAGE_CODE = 'MA'
                      AND KCT.ROOM_TYPE = 'D'
                      AND DSV.SEAT_KEY = SPT.REC_KEY
                      AND SPT.CORNER_CODE = KCT.CODE
                      AND DSV.USER_KEY = CLU.REC_KEY(+)
                      AND SPT.ROOM_CODE = KRT.CODE
                      AND DSV.START_TIME BETWEEN(TO_DATE('20170325', 'YYYY/MM/DD'))
                      AND(TO_DATE('20170325', 'YYYY/MM/DD') + 1)
                      AND KCT.CODE = '602'
                      AND KRT.CODE IN('001')
                      AND KCT.CODE IN('600', '601', '602', '603')
                  ) BODY,
                  (
                    SELECT
                      (TO_DATE('20170325', 'YYYYMMDD') + LEVEL - 1) AS DAY_LIST
                    FROM
                      DUAL CONNECT BY(TO_DATE('20170325', 'YYYYMMDD') + LEVEL - 1) <= TO_DATE('20170325', 'YYYYMMDD')
                  ) ROW_ALIAS
                WHERE
                  TO_CHAR(ROW_ALIAS.DAY_LIST, 'YYYYMMDD') = TO_CHAR(BODY.START_TIME(+), 'YYYYMMDD')
                GROUP BY
                  TO_CHAR(ROW_ALIAS.DAY_LIST, 'YYYY/MM/DD')
                ORDER BY
                  TO_CHAR(ROW_ALIAS.DAY_LIST, 'YYYY/MM/DD')"
                ,QueryForm.GetCornerQueryString(QueryForm.Corner.DVD상영실, new DateTime(2017, 3, 25), "MA"));
        }
    }
}