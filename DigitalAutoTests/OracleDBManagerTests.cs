﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DigitalAuto.Model.DAO;
using System;
using System.Collections.Generic;

namespace DigitalAuto.Model.DAO.Tests {
    [TestClass()]
    public class OracleDBManagerTests {

        readonly string Ip = DigitalAutoTests.Properties.Settings.Default.ip;
        readonly uint Port = DigitalAutoTests.Properties.Settings.Default.port;
        readonly string dbUserId = DigitalAutoTests.Properties.Settings.Default.dbUserId;
        readonly string dbSID = DigitalAutoTests.Properties.Settings.Default.dbSID;
        readonly string dbPassword = DigitalAutoTests.Properties.Settings.Default.dbPassword;

        OracleDBManager oracleDBManager;

        [TestInitialize()]
        public void Initialize() {
            oracleDBManager = new OracleDBManager(Ip, Port);
            oracleDBManager.ConnectionDB(dbUserId, dbSID, dbPassword);
        }

        [TestMethod()]
        public void QueryTest() {
            List<String[]> output = oracleDBManager.Query("Select * From LAPTOP");
            Assert.AreEqual("11.30", output[4][4]);
        }

        [TestCleanup()]
        public void CleanUp() {
            oracleDBManager.Close();
        }


    }
}