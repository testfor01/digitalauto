﻿using DigitalAuto.Model.Data;
using DigitalAuto.Model.Templete;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Newtonsoft.Json;
using System.IO;

namespace DigitalAuto.Tests {

    [TestClass()]
    public class ExcelManagerTests {

        ExcelManager excelManager;
        ExcelTempleteObject templete;

        AgeGender ageGender;
        Corner corner;

        [TestInitialize()]
        public void Initialize() {
            const string pathfile = "D:\\Documents\\Visual Studio 2017\\Projects\\DigitalAuto\\DigitalAutoTests\\test.xlsx";
            const string templetePath = "D:\\Documents\\Visual Studio 2017\\Projects\\DigitalAuto\\DigitalAutoTests\\test_templete.xlsx";
            const uint defaultSheetNo = 1;
            const uint totalSheetNo = 2;
            excelManager = new ExcelManager(pathfile, templetePath, defaultSheetNo, totalSheetNo);
            ageGender = new AgeGender(11, 22, 33, 44, 55, 66);
            corner = new Corner(10, 20, 30, 40);
            JsonSerializer serializer = new JsonSerializer();
            JsonReader reader = new JsonTextReader(new StreamReader("D:\\Documents\\Visual Studio 2017\\Projects\\DigitalAuto\\DigitalAutoTests\\test_templete.json"));
            templete = serializer.Deserialize<ExcelTempleteObject>(reader);
        }

        [TestMethod()]
        public void ExcelManagerTest() {
            excelManager.BuildDefaultWorkSheet(excelManager.CreateDefaultWorkSheet(), templete, new DateTime(2019, 1, 23), ageGender, corner);
            excelManager.BuildDefaultWorkSheet(excelManager.CreateDefaultWorkSheet(), templete, new DateTime(2019, 1, 24), ageGender, corner);
            excelManager.BuildDefaultWorkSheet(excelManager.CreateDefaultWorkSheet(), templete, new DateTime(2019, 1, 25), ageGender, corner);
            excelManager.BuildTotalWorkSheet(excelManager.CreateTotalWorkSheet(), templete, new DateTime(2019, 1, 23), new DateTime(2019, 1, 25));
        }

        [TestCleanup()]
        public void Cleanup() {
            excelManager.Close();
        }

    }
}